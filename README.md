# VoipMS-SMS-Matrix

This is a python3 webapp using uswgi/nginx to send and recieve SMS messages via 
voip.ms. 

To send a new SMS message, open a new matrix room and invite the SMS bot. Set 
the topic of the room with the phone number and start chatting. 

Incomming messages will be recived when voip.ms hits a specific URL. The bot 
will look for a room with a topic the same as the source phone number. If the 
bot finds a room it will post the message there. If the bot does not match the 
source phone number it will create a new room, invite the user, and send the 
message to the new room.

This is my first ever program. If there is anyting you can fix please send a 
merge request! I'll test and merge it.

I've tried to keep all the personal information in the "debug"(10) level of the 
logs. Info (20) should tell you what it's doing without logging sms content or 
phone numbers.

## Requires:

python3  
https://github.com/matrix-org/matrix-python-sdk  
apache/nginx  
uwsgi  
https://pypi.org/project/voipms/

## Install  

yum install epel-release.noarch  
yum install python34 python34-pip gcc python34-devel nginx wget  
pip3 install  --upgrade pip  
pip3 install virtualenv

**#where our virtualenv will live**  
mkdir -p /usr/share/nginx/python/voipms-sms-matrix

**#for the socket file**  
mkdir /run/uwsgi/

**#update owner to be the user we want this running as**  
chown -R nginx:nginx /usr/share/nginx/python  
chown nginx:nginx /run/uwsgi/


**#We dont want to run this as the root user.**  
su - nginx -s /bin/bash  

**#Set up the python virtual Environment**  
cd /usr/share/nginx/python/voipms-sms-matrix  
virtualenv voipmsenv  
source voipmsenv/bin/activate  
pip3 install uwsgi  
pip3 install voipms  
pip3 install matrix_client  

**#Grab the config for uwsgi**  
cd /usr/share/nginx/python/voipms-sms-matrix  
wget https://gitlab.com/untidylamp/VoipMS-SMS-Matrix/raw/master/voipms.ini

**#change the log files if you want**  
vi /usr/share/nginx/python/voipms-sms-matrix/voipms.ini  

**#Grab the main program - Edit and update vars at the top**  
wget https://gitlab.com/untidylamp/VoipMS-SMS-Matrix/raw/master/matrixvoipms.py  
vi /usr/share/nginx/python/voipms-sms-matrix/matrixvoipms.py  

**#exit out of user to root**  
exit  


**#tell nginx where the socket file will be for uwsgi**  
*#in this example I'll use the 80 default_server but you should set up SSL*  

vi /etc/nginx/nginx.conf

**#add the below bit of code under `server {` like the default `location /` code**  
```
  location /voipms {
    include uwsgi_params;
    uwsgi_pass unix:/run/uwsgi/voipms.sock;
  }
```

**#Grab the service file so we can start on boot**  
cd ~  
wget https://gitlab.com/untidylamp/VoipMS-SMS-Matrix/raw/master/voipms-sms-matrix.service  
mv voipms-sms-matrix.service /etc/systemd/system/  
systemctl start voipms-sms-matrix.service  
systemctl enable voipms-sms-matrix.service  
systemctl enable nginx  
systemctl start nginx  


`curl https://127.0.0.1/voipms?to\=1235550987\&from\=1235550987\&message\=Hello World\&id\=0001\&date\=2018-06-17\&secret\=changeme`


**#Check for Errors**  

less /tmp/myapp.log  
less /tmp/errlog  


## Voip ms setup


**#login to voip.ms** 

main menu -> SOAP and REST/JSON API ( https://voip.ms/m/api.php )  
set a password, save.  
enable API, save.  
set your servers IP Address, save.

did numbers -> Manage DID(s)  (https://voip.ms/m/managedid.php)  
edit the did with SMS enabled   
Short Message Service (SMS): yes, check this box  
SMS URL Callback: `https://example.ca/voipms?secret=changeme&to={TO}&from={FROM}&message={MESSAGE}&id={ID}&date={TIMESTAMP}`  
URL Callback Retry: yes, check this box.


## Contributing:

Feel free to send requests. If it works I'll merge it.