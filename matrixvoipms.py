matrix_url = "http://127.0.0.1:8008"
matrix_user = "@YOU:example.ca"
matrix_bot = "@BOT:example.ca"
matrix_bot_password = "password"

secret_key = "changeme"

voipms_username = "email@example.ca"
voipms_api_password = "Passworddd"
voipms_did = 1235550987

sms_confirm_sent = False

log_path = '/tmp/myapp.log'
log_level = 10
log_size_bytes = 10000000
log_backup_count = 2


#Do not edit below

#Imports required for logging
import logging
import logging.handlers as handlers

#Imports for threading
import threading

#Import for sleeping second thread
import time

#import to parse URL from VoipMS
from urllib import parse

#Used for splitting messages to ~154 for VoipMsSend
from math import ceil

#Imports for VoipMS
from voipms import VoipMs

#for username and password loginto get token
from matrix_client.client import MatrixClient
#main program is in MatrixHttpApi
from matrix_client.api import MatrixHttpApi



##Setup Logging##
lock_log = threading.Lock()

logger = logging.getLogger('VoipMS-SMS-Matrix')
logger.setLevel(logging.DEBUG)

# Here we define our formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

log_handler = handlers.RotatingFileHandler(log_path, maxBytes=log_size_bytes, backupCount=log_backup_count)
log_handler.setLevel(log_level)
log_handler.setFormatter(formatter)

logger.addHandler(log_handler)





#Test URL data, called from def UrlLoad
def TestUrl(request):
  url_request = parse.parse_qs(request)

  try: url_request['secret']
  except:
    with lock_log:
      logger.error('def TestUrl - missing \'secret=value\' in URL')
    return 1

  try: url_request['date']
  except:
    with lock_log:
      logger.error('def TestUrl - missing \'date=value\' in URL')
    return 2

  try: url_request['message']
  except:
    with lock_log:
      logger.error('def TestUrl - missing \'message=value\' in URL')
    return 3

  try: url_request['to']
  except:
    with lock_log:
      logger.error('def TestUrl - missing \'to=value\' in URL')
    return 4

  try: url_request['from']
  except:
    with lock_log:
      logger.error('def TestUrl - missing \'from=value\' in URL')
    return 5

  try: url_request['id']
  except:
    with lock_log:
      logger.error('dev TestUrl - missing \'id=value\' in URL')
    return 6

  return 0


def GetRooms(matrix_client):
 dict_rooms=matrix_client._send("GET", "/joined_rooms")
 return dict_rooms['joined_rooms']


def GetTopic(matrix_client, room_id):
  dict_topic = {}
  try: dict_topic=matrix_client._send("GET", "/rooms/"+room_id+"/state/m.room.topic")
  except: dict_topic['topic'] = "failed"
  return dict_topic['topic']


def GetRoomName(matrix_client, room_id):
  dict_name = {}
  #try: 
  dict_name=matrix_client._send("GET", "/rooms/"+room_id+"/state/m.room.name")
  #except: dict_name['name'] = "failed"
  return dict_name['name']


def SetDisplayName(matrix_client, room_id, display_name):
  #/_matrix/client/unstable/rooms/{roomId}/state/m.room.member/{stateKey}
  #with lock_log:
  #  logger.error('room_id ' + room_id )
  json_body = { 'membership': 'join', 'displayname': display_name}
  matrix_client._send("PUT", "/rooms/"+room_id+"/state/m.room.member/"+matrix_bot, json_body)
  return 0


def LeaveRoom(matrix_client, room_id):
  matrix_client._send("POST", "/rooms/"+room_id+"/leave")
  return 0


def GetInvites(matrix_client, sync_info):
  invites = []
  for types, content in sync_info['rooms'].items():
    if types == "invite":
      for room in content:
        sender = content[room]['invite_state']['events'][0]['sender']
        if sender == matrix_user:
          with lock_log:
            logger.debug('def GetInvites - invite for room: %s',room)
          invites.append(room)

  return invites

def JoinRooms(matrix_client, invites):
  for invite in invites:
    with lock_log:
      logger.debug('def JoinRooms - trying to join: %s',invite)
    path=str("/join/"+invite)
    method="POST"
    try: matrix_client._send(method=method, path=path)
    except Exception as problem:
      with lock_log:
        logger.error('JoinRooms: %s',problem)

  return 0

#Send Messages to Matrix Room
def MatrixSend(matrix_client, room_id, event_type, content_pack):
  with lock_log:
    logger.info('def MatrixSend - Trying to send message to Matrix Room')

  matrix_id = matrix_client.send_message_event(room_id=room_id,event_type=event_type,content=content_pack)

  with lock_log:
    logger.info('def MatrixSend - Message Sent to room.')

  with lock_log:
    logger.debug('def MatrixSend - Room %s. Event ID %s. Content %s',room_id,matrix_id['event_id'],content_pack)

  return matrix_id

##return sync time for new messages
def MatrixFirstSync(matrix_client):

  sync_info = matrix_client.sync(since=0)
 
  return sync_info["next_batch"]



def GetMatrixMessages(matrix_client, sync_info):
  outbound_messages = {}
  #for each room we're in check for new messages and store them in a Dict.

  for room_id, syncRoom in sync_info['rooms']['join'].items():
    outbound_messages[room_id] = []
    for event in syncRoom['timeline']['events']:
      if event['sender'] == matrix_user and event['type'] == 'm.room.message':
        with lock_log:
          logger.info('def GetMatrixMessages - found a message in matrix for sending to voip.ms')
        with lock_log:
          logger.debug('def GetMatrixMessages - Room ID %s - Message: %s',room_id,event['content']['body'])
        outbound_messages[room_id].append(event['content']['body'])

    #If there's no value (m.room.message) for the key (room_id) remove it
    if not outbound_messages[room_id]:
      outbound_messages.pop(room_id, None)

  return outbound_messages


def VoipMsSend(voipms_client, number, message):
  voipms_send = {}

  #limitation of voipms - Messages can not be longer than 160 char
  number_of_chunks = ceil(len(message)/154)

  if number_of_chunks > 1:
    chunk = 1

    for count in range(0, len(message), 154):
      #will break each message into chunks and add (?/?) at the end
      message_in_chuncks = message[count:count+154] + " (" + str(chunk) + "/" + str(number_of_chunks) + ")"

      #try to send the message
      try:
        #TODO - if the last message in a chunk passes but the rest fail it will still return success
        voipms_send = voipms_client.dids.send.sms(voipms_did, number, message_in_chuncks)
        with lock_log:
          logger.info('def VoipMsSend - SMS Message sent to phone')
        with lock_log:
          logger.debug('def VoipMsSend - To: %s - from %s - message: %s',number, voipms_did, message_in_chuncks)
      except Exception as problem:
        with lock_log:
          logger.error('def VoipMsSend - sms Send: %s',problem)
        voipms_send['status'] = problem
      chunk += 1

  else:
    try:
      voipms_send = voipms_client.dids.send.sms(voipms_did, number, message)
      with lock_log:
        logger.info('def VoipMsSend - SMS Message sent to phone')
      with lock_log:
        logger.debug('def VoipMsSend - To: %s - from %s - message: %s',number, voipms_did, message)
    except Exception as problem:
      with lock_log:
        logger.error('def VoipMsSend - sms Send: %s',problem)
      voipms_send['status'] = str(problem)

  return voipms_send


def MatrixRoomMembers(matrix_client, sync_info):

  #for each room we're in
  for room_id, syncRoom in sync_info['rooms']['join'].items():

    #Loop through timeline events
    for event in syncRoom['timeline']['events']:
      #If matrix_user leaves room
      if event['sender'] == matrix_user and event['type'] == 'm.room.member' and event['content']['membership'] == 'leave':

        #leave room
        with lock_log:
          logger.info('def WatchMatrixRooms - Leaving %s because user left.',room_id)
        LeaveRoom(matrix_client, room_id)

  return 0


#Second thread - sync with matrix server
def MatrixEvents(matrix_client,voipms_client,last_sync):

  with lock_log:
    logger.info('def MatrixEvents - Thread started')

  all_messages = []

  while True:

    with lock_log:
      logger.debug('def MatrixEvents - Sync with Matrix since %s',last_sync)

    #sync to server
    sync_info = matrix_client.sync(since=last_sync)
    #set new sync time
    last_sync=sync_info["next_batch"]

    #check for invites
    invites = GetInvites(matrix_client, sync_info)

    if invites:
      with lock_log:
        logger.info('def MatrixEvents - Found invites.')

      #try and join rooms that we're invited to.
      JoinRooms(matrix_client, invites)

    else:
      with lock_log:
        logger.info('def MatrixEvents - No invites')


    #get messages
    all_messages = GetMatrixMessages(matrix_client, sync_info)

    #for each message 
    if all_messages:

      with lock_log:
        logger.info('def MatrixEvents - Found messages')

      for room_id, room_messages in all_messages.items() :

        #Try to grab the number
        try: number=int(GetTopic(matrix_client, room_id))
        except:
          content_pack = {
            "msgtype": "m.text",
            "body": "Set topic to a phone number and try again.",
          }
          with lock_log:
            logger.warning('def MatrixEvents - Matrix Room %s. %s',room_id,content_pack['body']) 
          MatrixSend(matrix_client, room_id, "m.room.message", content_pack)
          #break from this room_id for loop
          break

        #for each message found
        for message in room_messages :

          with lock_log:
            logger.debug('def MatrixEvents - Matrix Room: %s - Phone number: %s - Message: %s',room_id,number,message)

          #Try to send the message to a Phone Number
          voipms_status=VoipMsSend(voipms_client,number,message)

          #if user wants to see confirmation sms was sent, post in matrix
          if voipms_status['status'] == "success" and sms_confirm_sent:
            content_pack = {
              "msgtype": "m.text",
              "body": "√",
              "voip.ms_SentTo": voipms_status['status'],
              "voip.ms_smsID": voipms_status['sms'],
            }
            MatrixSend(matrix_client, room_id, "m.room.message", content_pack)
          elif voipms_status['status'] != "success":
            content_pack = {
              "msgtype": "m.text",
              "body": voipms_status['status'],
              "voip.ms_SentTo": voipms_status['status'],
            }
            MatrixSend(matrix_client, room_id, "m.room.message", content_pack) 


    else:
      with lock_log:
        logger.info('def MatrixEvents - No messages')

    #Watch room to see if user left
    MatrixRoomMembers(matrix_client, sync_info)

    #Done with the Sync info.
    with lock_log:
      logger.info('def MatrixEvents - sleeping.')
    time.sleep(15)


  #We should never get here
  return 1


#VoipMS will load the URL and this will run
def UrlLoad(env, start_response):


  # Grab url query and print it to log
  with lock_log:
    logger.info('def UrlLoad - incoming message')
  with lock_log:
    logger.debug('def UrlLoad - request url: %s',str(env['QUERY_STRING']))


  #test url for each var, display 400 if it's missing information
  url_result = TestUrl(env['QUERY_STRING'])
  if url_result:
    html_return = bytes("400", 'utf-8')
    html_status = '400 Bad Request'
    response_headers = [('Content-type', 'text/html')]
    start_response(html_status, response_headers)
    return [html_return]

  with lock_log:
    logger.info('def UrlLoad - URL has all information')

  #URL is okay. Parse it
  voipms_url = parse.parse_qs(env['QUERY_STRING'])

  #Grab seret and test it
  secret = str(voipms_url['secret'])[2:-2]

  if secret!=secret_key:
    with lock_log:
      logger.warn('def UrlLoad - Secret missmatch. found \'%s\' in url. Please check config', secret)
    html_return = bytes("400", 'utf-8')
    html_status = '403 Forbidden'
    response_headers = [('Content-type', 'text/html')]
    start_response(html_status, response_headers)
    return [html_return]

  with lock_log:
    logger.info('def UrlLoad - Secret matches')

  # secret okay, fishish setting vars
  message = str(voipms_url['message'])[2:-2]
  voipms_to = str(voipms_url['to'])[2:-2]
  voipms_from = str(voipms_url['from'])[2:-2]
  voipms_id = str(voipms_url['id'])[2:-2]
  voipms_time = str(voipms_url['date'])[2:-2]

  #Now that we have the message information, check for a room
  found_room = False
  matched_matrix_room = ""

  for room_id in GetRooms(matrix_client):
    if voipms_from == GetTopic(matrix_client, room_id):
      found_room = True
      matched_matrix_room = room_id
      with lock_log:
        logger.debug('def UrlLoad - VoipMS From number %s matches Room %s Topic.', voipms_from, room_id)
      
  if not found_room:
    with lock_log:
      logger.info('def UrlLoad - No room found, going to make one')

    #make room
    new_room = matrix_client.create_room(is_public=False)
    #set topic
    matrix_client.set_room_topic(new_room['room_id'], voipms_from)
    #set name
    matrix_client.set_room_name(new_room['room_id'], voipms_from)

    #invite user
    try: matrix_client.invite_user(new_room['room_id'], matrix_user)
    except Exception as problem:
      with lock_log:
        logger.debug('def UrlLoad - failed to invite user to new room %s',problem)

    #set up matched_matrix_room so we can send a message
    matched_matrix_room = new_room['room_id']
    with lock_log:
      logger.debug('def UrlLoad - created new room: %s',matched_matrix_room)

  #get Name of room
  room_name = GetRoomName(matrix_client, matched_matrix_room)
  #Test if same

  #set name of room
  SetDisplayName(matrix_client, matched_matrix_room, room_name)

  #build content to send to matrix server
  content_pack = {
    "msgtype": "m.text",
    "body": message,
    "voip.ms_To": voipms_to,
    "voip.ms_From": voipms_from,
    "voip.ms_ID": voipms_id,
    "voip.ms_Time": voipms_time,
  }

  MatrixSend(matrix_client, matched_matrix_room, "m.room.message", content_pack)




  # End From VoipMS to Matrix
  ###########################
  # HTML PAGE Stuff

  html_return = bytes("ok", 'utf-8')
  html_status = '200 OK'
  response_headers = [('Content-type', 'text/html')]
  start_response(html_status, response_headers)
  return [html_return]



##Main
with lock_log:
  logger.info('Main - starting...')


#Login to Matrix
login = MatrixClient(matrix_url)
#TODO: test this some how

try:
  token = login.login_with_password(username=matrix_bot, password=matrix_bot_password)
  with lock_log:
    logger.debug('Main - Matrix token is %s',token)
  matrix_client = MatrixHttpApi(matrix_url, token=token)
except:
  with lock_log:
    logger.critical('Main - something wrong with Matrix login')

with lock_log:
  logger.info('Main - Matrix login seems okay')


#Login to VoipMS
voipms_client = VoipMs(voipms_username, voipms_api_password)
try: 
  voipms_client.general.get.ip()
  logger.info('Main - voip.ms login looks good')
except Exception as problem:
  with lock_log:
    logger.critical('Main - voip.ms: %s',problem)


#First sync grab current sync time
sync_time = MatrixFirstSync(matrix_client)

with lock_log:
  logger.debug('Main - Staring sync time: %s',sync_time)


#Thread for syncing with server to get messages
#The main thread waits for messages from VoipMS (def UrlLoad)
#The new thread will sync with Matrix (def CheckEvents)
thread_list = []
with lock_log:
  logger.debug('Main - before thread_list: %s',thread_list)

t = threading.Thread(target=MatrixEvents, args=(matrix_client,voipms_client,sync_time))
t.start()
thread_list.append(t)
with lock_log:
  logger.debug('Main - thread_list: %s',thread_list)